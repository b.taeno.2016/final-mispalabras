# Final MisPalabras

# Entrega practica

## Datos

* Nombre: Bruno Taeño Elez
* Titulación: Ingeniería Telemática
* Video básico (url): https://youtu.be/wa0fE-jsoFE 
* Video parte opcional (url): https://www.youtube.com/watch?v=g4vXjchIQjM 
* Despliegue (url): http://btaeno.pythonanywhere.com

## Cuenta Admin Site

* admin/admin
* btaeno/passpfinal

## Cuentas usuarios

* usuarioprueba/passpfinal
*  usuario1/passpfinal

## Resumen parte obligatoria

Funciona todo correctamente, en el despliegue no se podrá buscar/añadir
ninguna palabra que no se encuentre en la base de datos.

## Lista partes opcionales

* Quitar like dado anteriormente por esa misma cuenta.
* Eliminar su propia cuenta de usuario.
* Inclusión de Favicon.
* Borrar Palabras almacenadas por esa misma cuenta.
* XML/JSON de cada palabra.
* XML/JSON de los comentarios.
* Posibilidad de añadir imágenes a los comentarios.


