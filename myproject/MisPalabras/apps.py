from django.apps import AppConfig
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

class MisPalabrasConfig(AppConfig):
    name = 'MisPalabras'


class WikipediaHandler(ContentHandler):

    def __init__ (self):
        self.inContent = False
        self.content = ""

    # Función para extraer las palabras que queramos de wikipedia
    def startElement (self, name, attrs):
        if name == 'extract':
            self.inContent = True

    # Función para crear la definición de la palabra de wikipedia
    def endElement (self, name):
        global significado

        if name == 'extract':
            significado ="Significado: " + self.content + "."
        if self.inContent:
            self.inContent = False
            self.content = ""

    # Función que añade a nuestro content que lo hemos inicializado vacio le añadimos las letras de las palabras
    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars


class Wiki:

    def __init__(self, stream):
        self.parser = make_parser()
        self.handler = WikipediaHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    # Después de inicializar lo que necesitamos para python creamos la función con la variable goblara definición
    def significado(self):
        global significado
        significado = ""
        return significado
