
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Palabra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('palabra', models.CharField(max_length=64)),
            ],
        ),
        migrations.CreateModel(
            name='Voto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.TextField()),
                ('tipo', models.CharField(choices=[('PS', 'Positivo'), ('NG', 'Negativo')], max_length=2)),
                ('palabra', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='MisPalabras.palabra')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('usuario', models.TextField()),
                ('descripcion', models.TextField()),
                #('titulo', models.CharField(max_length=64)),
                ('fecha', models.DateTimeField(default=django.utils.timezone.now)),
                ('definicion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='MisPalabras.palabra')),
            ],
        ),
    ]
