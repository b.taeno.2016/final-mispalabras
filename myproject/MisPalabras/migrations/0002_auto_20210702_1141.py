
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MisPalabras', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='voto',
            name='tipo',
        ),
        migrations.AddField(
            model_name='comentario',
            name='titulo',
            field=models.CharField(default='No title', max_length=64),
        ),
        migrations.AddField(
            model_name='voto',
            name='valor',
            field=models.TextField(default='No valor'),
        ),
    ]
