from django.contrib import admin
from .models import Comentario, Voto, Palabra


# Register your models here.
admin.site.register(Comentario)
admin.site.register(Voto)
admin.site.register(Palabra)


