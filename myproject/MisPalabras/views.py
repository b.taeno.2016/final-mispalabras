import random
import urllib.request, json, xmltojson

import xmltodict
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from . import models
from django.utils import timezone
from django.http import HttpResponse
from bs4 import BeautifulSoup
import requests
import requests_html
from urllib.request import urlopen
from django.http import JsonResponse
from .apps import Wiki
from .models import Palabra, Comentario, Voto, Palabra
import xml.etree.ElementTree as ET


def search_wiki(name):
    url_def = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=%s&prop=extracts&exintro&explaintext" % name
    # hacemos la petición get a la API de la wikipedia con la url que hemos formado en url_def. Esta Url siempre va a ser igual a excepción de la palabra, por eso la he definido en una variable
    resp = requests.get(url_def)
    resp_xml = resp.text
    root = ET.fromstring(resp_xml)
    definicion = root.find("./query/pages/page/").text

    return definicion
def search_img(name):
    url_def = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=%s&prop=extracts&exintro&explaintext" % name
    # hacemos la petición get a la API de la wikipedia con la url que hemos formado en url_def. Esta Url siempre va a ser igual a excepción de la palabra, por eso la he definido en una variable
    resp = requests.get(url_def)
    resp_xml = resp.text
    root = ET.fromstring(resp_xml)
    id_pag = root.find("./query/pages/").attrib.get('pageid')
    foto_url = "https://es.wikipedia.org/w/api.php?action=query&titles=%s&prop=pageimages&format=json&pithumbsize=200" % name
    # hacemos la petición get a la API de la wikipedia con la url que hemos formado en foto_url. Esta Url siempre va a ser igual a excepción de la palabra, por eso la he definido en una variable
    resp_foto = requests.get(foto_url)
    json_foto = resp_foto.json()
    try:
        foto_source = json_foto.get('query').get('pages').get(id_pag).get('thumbnail').get('source')
    except:
        foto_source = ""
    return foto_source
def search_RAE(name):
    rae_url = "https://dle.rae.es/%s" % name
    user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
    headers = {'User-Agent': user_agent}
    req = urllib.request.Request(rae_url, headers=headers)
    with urllib.request.urlopen(req) as response:
        html = response.read().decode('utf8')
    soup = BeautifulSoup(html, 'html.parser')
    content = soup.find('meta', property="og:description")
    descripcion_rae = content['content']
    return descripcion_rae
def search_flickr(name):
    url_xml = "https://www.flickr.com/services/feeds/photos_public.gne?tags=" + name
    xml = urllib.request.urlopen(url_xml)
    xml_dict = xmltodict.parse(xml.read())
    flickr_photo = xml_dict["feed"]["entry"][0]["link"][1]["@href"]
    return flickr_photo
def ordenar_palabras(request, lista_palabras):
    sorted_words = list(range(10))
    sorted_words = sorted(lista_palabras, key=lambda x: x.votos, reverse=True)

    return sorted_words
def random_word(lista_palabras):
    random_pal = Palabra()
    try:
        lista_palabras = Palabra.objects.all()
        random_pal = Palabra()
        random_pal = random.choice(lista_palabras)
    except:
        random_pal = ""
    return random_pal
@csrf_exempt
def pagina_palabra(request, llave):
    palabra = Palabra()
    random_pal = ""
    existent = True
    voted = False

    lista_palabras = Palabra.objects.all()
    lista_palabras_slice2 = lista_palabras[0:3]
    lista_comentarios = (Comentario.objects.all())
    random_pal = random_word(lista_palabras)
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)

    lista_votos = list(Voto.objects.all())
    for v in lista_votos:
        if (v.usuario == request.user.username) and (v.palabra.pal == llave):
            voted = True

    try:
        palabra = Palabra.objects.get(pal=llave)

    except Palabra.DoesNotExist:
        palabra = Palabra(pal=llave,
                          definicion=search_wiki(llave),
                          definicion_rae=search_RAE(llave),
                          imagen=search_img(llave),
                          imagen_flickr=search_flickr(llave),
                          usuario=request.user.username,
                          votos=0, Num_comentarios=0, fecha=timezone.now())
        existent = False
        palabra.save()
    if request.method == "PUT":

        definicion = request.body.decode('utf-8')
    if request.method == "POST":
        action = request.POST['action']

        if action == "Editar palabra":
            try:
                palabra = Palabra.objects.get(pal=llave)
                palabra.definicion = request.POST['definicion']
                palabra.save()
            except Palabra.DoesNotExist:
                palabra = Palabra(pal=llave, definicion=request.POST['definicion'])
                palabra.save()
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                palabra = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Enviar palabra":
            pal = llave
            try:
                palabra = Palabra.objects.get(pal=pal)
                palabra.delete()
            except Palabra.DoesNotExist:
                pass
            palabra = Palabra(pal=pal, definicion=request.POST['definicion'],
                                  usuario=request.user.username,
                                  votos=0, Num_comentarios=0, fecha=timezone.now())
            palabra.save()
        elif action == "LIKE":
            palabra = Palabra.objects.get(pal=llave)
            palabra.votos += 1
            palabra.save()
            voto = Voto(palabra=Palabra.objects.get(pal=llave), usuario=request.user.username,
                        valor="like")
            voto.save()
            return redirect('/' + llave)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Inicio de sesion":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user:
                login(request, user)
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')

        elif action == "Borrar palabra":
            try:
                palabra = Palabra.objects.get(pal=llave)
                palabra.delete()
                return redirect('/')
            except Palabra.DoesNotExist:
                palabra = Palabra.objects.get(pal=llave, definicion=definicion)
                palabra.save()
        elif action == "Añadir esta palabra":
            return redirect('/')
        elif action == "No añadir palabra":
            try:
                palabra = Palabra.objects.get(pal=llave)
                palabra.delete()
                return redirect('/')
            except Palabra.DoesNotExist:
                palabra = Palabra.objects.get(pal=llave, definicion=definicion)
                palabra.save()
        elif action == "Volver e iniciar sesión":
            try:
                palabra = Palabra.objects.get(pal=llave)
                palabra.delete()
                return redirect('/')
            except Palabra.DoesNotExist:
                palabra = Palabra.objects.get(pal=llave, definicion=definicion)
                palabra.save()
        elif action == "Volver y registrarme":
            try:
                palabra = Palabra.objects.get(pal=llave)
                palabra.delete()
                return redirect('/registro')
            except Palabra.DoesNotExist:
                palabra = Palabra.objects.get(pal=llave, definicion=definicion)
                palabra.save()

        elif action == "Página principal":
            return redirect('/')

        elif action == "Perfil usuario":
            return redirect('usuario/<request.user.username>')
        elif action == "Ayuda":
            return redirect('/ayuda')
        elif action == "Borrar like":
            palabra = Palabra.objects.get(pal=llave)
            voto = Voto.objects.get(palabra=Palabra.objects.get(pal=llave),
                                    usuario=request.user.username)
            palabra.votos -= 1
            palabra.save()
            voto.delete()
            return redirect('/' + llave)
        elif action == "Enviar comentario":
            palabra = Palabra.objects.get(pal=llave)
            palabra.Num_comentarios += 1
            coment = Comentario(palabra=palabra, titulo=request.POST['titulo'],
                                descripcion=request.POST['descripcion'],
                                url=request.POST['url'], fecha=timezone.now(), usuario=request.user.username)
            coment.save()
            palabra.save()
    try:
        Palabra.objects.get(pal=llave)
    except Palabra.DoesNotExist:
        if request.GET.get("format") == "xml/":
            contexto = {'Claves': Palabra.objects.all()}
            return HttpResponse(render(contexto, request))
        if request.GET.get("format") == "json/":
           palabra = Palabra.objects.filter().values()
           return JsonResponse({"Palabra": list(palabra)})

        lista_palabras = Palabra.objects.all()
        lista_palabras_slice2 = lista_palabras[0:3]
        random_pal = random_word(lista_palabras)
        contexto = {
            'voted': voted,
            'random_pal': random_pal,
            'lista_palabras_slice2': lista_palabras_slice2}
        return render(request, 'MisPalabras/pagina_palabra_no_enc.html', contexto)

    palabra = Palabra.objects.get(pal=llave)
    url = palabra.definicion
    headers = {'User-Agent': 'Mozilla/5.0'}
    try:
        html_page = requests.get(url, headers=headers)
        Bsoup = BeautifulSoup(html_page.text, 'html.parser')
        images = Bsoup.findAll('img')
        title = Bsoup.find("title").text
        imagen = ((images[1])['src'])
        contexto = {
            'n_palabras': iterador,
            'voted': voted,
            'palabra': palabra,
            'random_pal': random_pal,
            'lista_palabras_slice2': lista_palabras_slice2,
            'lista_comentarios': lista_comentarios,
            'lista_votos': lista_votos,
            'sorted_words': sorted_words,
            'title': title,
            'imagen': imagen,
        }
        return render(request, 'MisPalabras/pagina_palabra_no_enc.html', contexto)
    except:
        contexto = {
            'n_palabras': iterador,
            'voted': voted,
            'palabra': palabra,
            'existent': existent,
            'random_pal': random_pal,
            'lista_palabras_slice2': lista_palabras_slice2,
            'sorted_words': sorted_words,
            'lista_comentarios': lista_comentarios,
            'lista_votos': lista_votos,
        }
    return render(request, 'MisPalabras/pagina_palabra.html', contexto)


@csrf_exempt
def index(request):
    existent = True
    random_pal = ""
    if request.method == "POST":
        action = request.POST['action']

        if action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                palabra = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Iniciar sesión":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"

        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Ayuda":
            return redirect('/ayuda')
        elif action == "Perfil usuario":
            return redirect('usuario/' + request.user.username)

    lista_palabras = list(reversed(Palabra.objects.all()))
    lista_palabras_slice = lista_palabras[0:10]
    lista_palabras_slice2 = lista_palabras[0:3]
    random_pal = random_word(lista_palabras)
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)
    aportaciones = 10
    contexto = {
        'n_palabras': iterador,
        'existent': existent,
        'random_pal': random_pal,
        'lista_palabras_slice': lista_palabras_slice,
        'lista_palabras': lista_palabras,
        'sorted_words': sorted_words,
        'aportaciones': aportaciones,
        'iterador': iterador,
        'lista_palabras_slice2': lista_palabras_slice2,
    }
    return render(request, 'MisPalabras/index.html', contexto)

def pagina_registro(request):

    form = UserCreationForm()
    random_pal = ""

    if request.method == "POST":
        action = request.POST['action']
        if action == "Página principal":
            return redirect('/')
        elif action == "Quiero registrarme":
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                new_user = authenticate(request, username=request.POST["username"], password=request.POST["password1"])
                login(request, new_user)
                return redirect('/')
        elif action == "Ayuda":
            return redirect('/ayuda')
    lista_palabras = list(reversed(Palabra.objects.all()))
    random_pal = random_word(lista_palabras)
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    contexto = {
        'n_palabras': iterador,
        'random_pal': random_pal,
        'lista_palabras': lista_palabras,
        'form': form
    }
    return render(request, 'MisPalabras/registro.html', contexto)
def pagina_usuario(request, llave):
    existent = True
    random_pal = ""

    if request.method == "POST":
        action = request.POST['action']
        if action == "Página principal":
            return redirect('/')
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                palabra = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Ayuda":
            return redirect('/ayuda')
        elif action == "Borrar cuenta":
            return redirect('/usuario/' + request.user.username + '/delete')
    lista_palabras = list(reversed(Palabra.objects.all()))
    random_pal = random_word(lista_palabras)
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    lista_palabras_slice2 = lista_palabras[0:3]
    lista_comentarios = Comentario.objects.all()
    lista_votos = Voto.objects.all()
    contexto = {
        'n_palabras': iterador,
        'existent': existent,
        'random_pal': random_pal,
        'lista_palabras': lista_palabras,
        'lista_comentarios': lista_comentarios,
        'lista_votos': lista_votos,
        'lista_palabras_slice2': lista_palabras_slice2,
    }
    return render(request, 'MisPalabras/pagina_usuario.html', contexto)


def pagina_borrado_usuario(request, llave):
    if request.method == "POST":
        action = request.POST['action']
        if action == "Página principal":
            return redirect('/')
        elif action == "Ayuda":
            return redirect('/ayuda')
        elif action == "Confirmar borrado":
            try:
                u = User.objects.get(username=request.user.username)
                u.delete()
                return redirect('/')
            except User.DoesNotExist:
                return redirect('/')

    lista_palabras = list(reversed(Palabra.objects.all()))
    random_pal = random_word(lista_palabras)
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1

    contexto = {
        'n_palabras': iterador,
        'random_pal': random_pal,
        'lista_palabras': lista_palabras,
    }
    return render(request, 'MisPalabras/pagina_borrado_usuario.html', contexto)


def pagina_ayuda(request):

    existent = True
    random_pal = ""

    if request.method == "POST":
        action = request.POST['action']
        if action == "Página principal":
            return redirect('/')
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                palabra = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Iniciar sesión":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"
        elif action == "Perfil usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Ayuda":
            return redirect('/ayuda')

    lista_palabras = list(reversed(Palabra.objects.all()))
    random_pal = random_word(lista_palabras)
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    lista_palabras_slice2 = lista_palabras[0:3]
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)

    contexto = {
        'n_palabras': iterador,
        'existent': existent,
        'random_pal': random_pal,
        'lista_palabras': lista_palabras,
        'lista_palabras_slice2': lista_palabras_slice2,
        'sorted_words': sorted_words
    }
    return render(request, 'MisPalabras/pagina_ayuda.html', contexto)


def memecharliesheen(request, palabra):
    texto_meme = ""
    if request.method == "POST":
        action = request.POST["action"]
        if action == "Texto abajo":
            texto_meme = request.POST["texto"]
        elif action == "Página principal":
            return redirect('/')
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                p = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Iniciar sesión":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"
        elif action == "Perfil usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Ayuda":
            return redirect('/ayuda')

    p = Palabra.objects.get(pal=palabra)
    lista_palabras = Palabra.objects.all()
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    random_pal = Palabra()
    random_pal = random.choice(lista_palabras)
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)


    context = {
               'n_palabras': iterador,
               'bottom_text': texto_meme,
               'palabra': p,
               'random_pal': random_pal,
               'sorted_words': sorted_words
               }
    return render(request, "MisPalabras/memecharliesheen.html", context)

def memedeadpool(request, palabra):
    texto_meme = ""
    if request.method == "POST":
        action = request.POST["action"]
        if action == "Texto abajo":
            texto_meme = request.POST["texto"]
        elif action == "Página principal":
            return redirect('/')
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                p = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Iniciar sesión":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"
        elif action == "Perfil usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Ayuda":
            return redirect('/ayuda')

    p = Palabra.objects.get(pal=palabra)
    lista_palabras = Palabra.objects.all()
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    random_pal = Palabra()
    random_pal = random.choice(lista_palabras)
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)

    context = {
        'n_palabras': iterador,
        'bottom_text': texto_meme,
        'palabra': p,
        'random_pal': random_pal,
        'sorted_words': sorted_words
    }

    return render(request, "MisPalabras/memedeadpool.html", context)


def memeoso(request, palabra):
    texto_meme = ""
    if request.method == "POST":
        action = request.POST["action"]
        if action == "Texto abajo":
            texto_meme = request.POST["texto"]
        elif action == "Página principal":
            return redirect('/')
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                p = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Iniciar sesión":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"
        elif action == "Perfil usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Ayuda":
            return redirect('/ayuda')

    p = Palabra.objects.get(pal=palabra)
    lista_palabras = Palabra.objects.all()
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    random_pal = Palabra()
    random_pal = random.choice(lista_palabras)
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)

    context = {
        'n_palabras': iterador,
        'bottom_text': texto_meme,
        'palabra': p,
        'random_pal': random_pal,
        'sorted_words': sorted_words
    }

    return render(request, "MisPalabras/memeoso.html", context)


def memecutecat(request, palabra):
    texto_meme = ""
    if request.method == "POST":
        action = request.POST["action"]
        if action == "Texto abajo":
            texto_meme = request.POST["texto"]
        elif action == "Página principal":
            return redirect('/')
        elif action == "Buscar palabra":
            n_palabra = request.POST['nombre']
            try:
                p = Palabra.objects.get(pal=n_palabra)
            except Palabra.DoesNotExist:
                existent = False
            return redirect('/' + n_palabra)
        elif action == "Registrarse":
            return redirect('/registro')
        elif action == "Cerrar sesión":
            logout(request)
            return redirect('/')
        elif action == "Iniciar sesión":
            username = request.POST["user"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o contraseña incorrectas"
        elif action == "Perfil usuario":
            return redirect('usuario/' + request.user.username)
        elif action == "Ayuda":
            return redirect('/ayuda')

    p = Palabra.objects.get(pal=palabra)
    lista_palabras = Palabra.objects.all()
    iterador = 0
    for palabra in lista_palabras:
        iterador += 1
    random_pal = Palabra()
    random_pal = random.choice(lista_palabras)
    sorted_words = list()
    sorted_words = ordenar_palabras(request, lista_palabras)

    context = {
        'n_palabras': iterador,
        'bottom_text': texto_meme,
        'palabra': p,
        'random_pal': random_pal,
        'sorted_words': sorted_words
    }

    return render(request, "MisPalabras/memecutecat.html", context)


def palabras_xml(request):
    msg = ""
    lista_palabras = list()
    try:
        lista_palabras = Palabra.objects.all()
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'lista_palabras': lista_palabras}

    return render(request, "MisPalabras/palabras.xml", content, content_type="text/xml")

def palabras_json(request):
    msg = ""
    lista_palabras = list()
    try:
        lista_palabras = Palabra.objects.all()
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'lista_palabras': lista_palabras}

    return render(request, "MisPalabras/palabras.json", content)

def palabra_xml(request, llave):
    msg = ""
    p = Palabra()
    try:
        p = Palabra.objects.get(pal=llave)
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'p': p}

    return render(request, "MisPalabras/palabra.xml", content, content_type="text/xml")

def palabra_json(request, llave):
    msg = ""
    p = Palabra()
    try:
        p = Palabra.objects.get(pal=llave)
    except Palabra.DoesNotExist:
        msg = "Todavía no hay palabras almacenadas"

    content = {'p': p}

    return render(request, "MisPalabras/palabra.json", content, content_type="text/json")

def comentarios_xml(request):
    msg = ""
    try:
        lista_comentarios = list(Comentario.objects.all())
    except:
        msg = "No hay ningún comentario para esta palabra"

    content = {'lista_comentario': lista_comentarios, 'msg': msg}

    return render(request, "MisPalabras/comentarios.xml", content, content_type="text/xml")

def comentarios_json(request):
    msg = ""
    try:
        lista_comentarios = list(Comentario.objects.all())
    except:
        msg = "No hay ningún comentario para esta palabra"

    content = {'lista_comentarios': lista_comentarios, 'msg': msg}

    return render(request, "MisPalabras/comentarios.json", content, content_type="text/json")

