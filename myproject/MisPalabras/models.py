from django.db import models

# Create your models here.

from django.db import models
from django.utils import timezone

# Create your models here.

class Palabra(models.Model):
    usuario = models.TextField(default='')
    pal = models.CharField(default='',max_length=50)
    definicion = models.TextField(default='')
    definicion_rae = models.TextField(default='')
    Num_comentarios = models.IntegerField(default=0)
    votos = models.IntegerField(default=0)
    fecha = models.DateTimeField(default=timezone.now)
    imagen = models.URLField(max_length=256, default="")
    imagen_flickr = models.URLField(max_length=256, default="")


#class Contenido(models.Model):
#    usuario = models.TextField()
#    pal = models.CharField(max_length=128) #clave
#    definicion = models.TextField() #valor
#    Num_comentarios = models.IntegerField()
#    votos = models.IntegerField()
#    fecha = models.DateTimeField(default=timezone.now)
#    imagen = models.URLField(max_length=256)



class Comentario(models.Model):
    usuario = models.TextField()
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    descripcion = models.TextField()
    url = models.TextField(default="No url")
    titulo = models.CharField(max_length=64, default="No title")
    fecha = models.DateTimeField(default=timezone.now)


class Voto(models.Model):
    usuario = models.TextField()
    palabra = models.ForeignKey(Palabra, on_delete=models.CASCADE)
    valor = models.TextField(default="No valor")


