"""MisPalabras URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, include
from .import views

urlpatterns = [
    path('',views.index),
    path('usuario/<str:llave>', views.pagina_usuario),
    path('usuario/<str:llave>/delete', views.pagina_borrado_usuario),
    path('ayuda', views.pagina_ayuda),
    path('registro', views.pagina_registro),
    path('format=xml', views.palabras_xml),
    path('format=json', views.palabras_json),
    path('comentarios/format=xml', views.comentarios_xml),
    path('comentarios/format=json', views.comentarios_json),
    path('<str:llave>/format=xml', views.palabra_xml),
    path('<str:llave>/format=json', views.palabra_json),
    path('<str:llave>', views.pagina_palabra),
    path('<str:palabra>/memecharliesheen', views.memecharliesheen),
    path('<str:palabra>/memedeadpool', views.memedeadpool),
    path('<str:palabra>/memeoso', views.memeoso),
    path('<str:palabra>/memecutecat', views.memecutecat),
]