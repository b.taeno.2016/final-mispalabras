
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MisPalabras', '0002_auto_20210702_1141'),
    ]

    operations = [
        migrations.AddField(
            model_name='comentario',
            name='url',
            field=models.TextField(default='No url'),
        ),
    ]
